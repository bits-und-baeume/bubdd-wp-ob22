#!/usr/bin/env python3
from xml.sax.saxutils import escape
import enum
import re


CANDIDATES = [
    "Jähnigen",
    "Pöhnisch",
    "Schulte-Wissermann",
    "Schollbach",
    "Pallas",
    "Hilbert",
]

class AspectAnswer(enum.Enum):
    MENTION = enum.auto()
    NOTHING = enum.auto()
    TOPIC = enum.auto()
    GOOD = enum.auto()
    FCK = enum.auto()

    @staticmethod
    def from_text(text):
        answer_by_text = {
            "e": AspectAnswer.MENTION,
            "ka": AspectAnswer.NOTHING,
            "th": AspectAnswer.TOPIC,
            "+": AspectAnswer.GOOD,
            "fck": AspectAnswer.FCK,
        }
        return answer_by_text[text]

    def color(self):
        color_by_answer = {
            AspectAnswer.MENTION: "#a1c854",
            AspectAnswer.NOTHING: "#ffeb57",
            AspectAnswer.TOPIC: "#ff3e3e",
            AspectAnswer.GOOD: "#a1c854",
            AspectAnswer.FCK: "#ff3e3e",
        }
        return color_by_answer[self]

    def text(self):
        text_by_answer = {
            AspectAnswer.MENTION: "erwähnt",
            AspectAnswer.NOTHING: "keine Aussage",
            AspectAnswer.TOPIC: "Thema verfehlt",
            AspectAnswer.GOOD: "erwähnt",
            AspectAnswer.FCK: "Regelverstoß",
        }
        return text_by_answer[self]

    def subtext(self):
        subtext_by_answer = {
            AspectAnswer.MENTION: "",
            AspectAnswer.NOTHING: "",
            AspectAnswer.TOPIC: "",
            AspectAnswer.GOOD: "★ gute Antwort",
            AspectAnswer.FCK: "",
        }
        return subtext_by_answer[self]


# load data

# questions
questions = []
with open("./data/fragen.txt") as f:
    for line in f:
        questions.append(line.strip())

# aspects
aspects = []
with open("./data/aspekte.txt") as f:
    data_re = re.compile(r"^\s*[|]\s*[0-9]+\s*[|].*$")
    for line_unstripped in f:
        line = line_unstripped.strip()
        if not data_re.match(line):
            continue

        fields = line.split("|")
        assert 10 == len(fields)

        answer_list = []
        for i in range(6):
            answer_text = fields[3 + i].strip()
            answer_list.append(AspectAnswer.from_text(answer_text))

        aspects.append(
            {
                "question_num": int(fields[1].strip()),
                "text": fields[2].strip(),
                "answers": answer_list,
            }
        )
        
# generate images

# header: questions
for question in questions:
    xml_question_str = escape(question).replace("'", "'\"'\"'")
    question_num = 1 + questions.index(question)
    # note: use > for sed, as it will be removed by escape()
    print(f"cat templates/header.svg | sed 's>FRAGE>{xml_question_str}>g' | inkscape -C -d 300 -o rendered/{question_num}_00_header.png /dev/stdin")

# body: individual aspects
for aspect in aspects:
    xml_aspect_text_str = escape(aspect["text"]).replace("'", "'\"'\"'")
    question_num = aspect["question_num"]
    aspect_num = 1 + aspects.index(aspect)

    print(f"cat templates/aspekt.svg | "
          f"sed '"
          f"s>ASPEKT>{xml_aspect_text_str}>g;",
          end="")
    for i in range(6):
        answer_num = 1 + i
        answer = aspect["answers"][i]
        color_str = escape(answer.color())
        text_str = escape(answer.text())
        subtext_str = escape(answer.subtext())
        color_searchmask = "#" + 6 * str(answer_num)
        print(f"s>TEXT{answer_num}>{text_str}>g;"
              f"s>{color_searchmask}>{color_str}>g;"
              f"s>SUB{answer_num}>{subtext_str}>g;",
              end="")
    print(f"' | inkscape -C -d 300 -o rendered/{question_num}_01_aspect_{aspect_num}.png /dev/stdin")




# footer: note to full text
for i in range(len(questions)):
    question_num = 1 + i
    print(f"cat templates/footer.svg | inkscape -C -d 300 -o rendered/{question_num}_02_footer.png /dev/stdin")

# splice images

for i in range(len(questions)):
    question_num = 1 + i
    print(f"convert -interpolate Catrom -append rendered/{question_num}_*.png rendered/done_{question_num}.png")

# generate info text
for i in range(len(questions)):
    question_num = 1 + i

    aspect_texts = []
    for aspect in aspects:
        if question_num != aspect["question_num"]:
            continue

        candidates_by_answer = {}
        for candidate, answer in zip(CANDIDATES, aspect["answers"]):
            if answer not in candidates_by_answer:
                candidates_by_answer[answer] = []
            candidates_by_answer[answer].append(candidate)

        # to string:
        texts = [f"Aspekt: {aspect['text']}"]
        for answer, candidates in candidates_by_answer.items():
            answer_text = answer.text()
            if "" != answer.subtext():
                answer_text += " ({})".format(answer.subtext())
            texts.append("{}: {}".format(
                answer_text,
                ", ".join(candidates)))
        aspect_texts.append("\n".join(texts))

    full_aspect_texts = "\n\n".join(aspect_texts)
    full_text = f"""Infografik, welche Kandidat:innen welche Aspekte bei ihrer Antwort angesprochen haben.
Frage: "{questions[i]}"

{full_aspect_texts}"""

    create_file = "echo -e '{}' > rendered/alt_{}.txt".format(
        full_text.replace("\n", "\\n").replace("'", "'\"'\"'"),
        question_num)
    print(create_file)
