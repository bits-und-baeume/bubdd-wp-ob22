Welche Ideen haben Sie, die Dresdner Bürger:innen mehr in demokratische Prozesse einzubeziehen?
Gibt es in Ihren Augen bereits jetzt digitale Hilfsmittel zur demokratischen Teilhabe in Dresden? Was würden Sie an der aktuellen Lage gern ändern oder ausbauen?
Haben Sie vor, die digitale Teilhabe für Bürger:innen zu erleichtern, die nicht Dienste von Monopol-Plattformen wie Twitter und Facebook nutzen wollen?
Halten Sie das Quasi-Monopol von Microsoft bei der Bereitstellung von verwaltungsrelevanter Software (Betriebssystem, Office) für problematisch und was planen Sie ggf. in Ihrer Amtszeit zu tun, um Abhängigkeiten zu reduzieren (Stichwort "Digitale Souveränität")?
Welche Möglichkeiten für die Stadt Dresden sehen Sie, sich für eine bessere Bildung im Bereich Digitalisierung und Nachhaltigkeit einzusetzen?
Welche Möglichkeiten sehen Sie, Bildungseinrichtungen bei Aufbau und Wartung digitaler Infrastruktur zu unterstützen, z.B. mit IT-Personal analog zu Hausmeister:innen?
Am 30.01.2020 hat der Stadtrat den  Antrag zur "Fortschreibung der Klimaschutzziele der Landeshauptstadt Dresden" (Umgangssprachlich: Klimanotstand) beschlossen. Wie ist Ihre Haltung dazu?
Welche Maßnahmen planen Sie im Bereich Müllvermeidung und Recycling, insbesondere auch in Bezug auf Elektroschrott?
Welchen Stellenwert (z.B. in Form von Haushaltsmitteln) hat IT-Sicherheit in Ihrem Regierungsprogramm?
Wie ist Ihre Haltung zur Verarbeitung von personenbezogenen und anderen kritischen Verwaltungsdaten mit sog. Cloud-Diensten wie z.B. Office365?
In der Verlängerung der Nutzungsdauer von Geräten liegt ein immenses Potenzial zur Ressourcenschonung. Offene Werkstätten, Repair-Cafés und Direktrecycling leisten dazu einen wichtigen Beitrag. Was beabsichtigen Sie zu tun, um diese Strukturen in Dresden zu stärken?
Wie stehen Sie zu dem Prinzip, dass in Bezug auf Softwarebeschaffung öffentliche Gelder vorrangig für Open-Source-Produkte (Stichwort: "Public Money – Public Code!") ausgegeben werden sollten?
