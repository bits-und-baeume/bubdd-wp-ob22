| Frage Nr. | Aspekt | jaehnigen | poehnisch | msw | schollbach | pallas | hilbert |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | Bürgerbeteiligungssatzung | e | ka | ka | ka | e | e | 
| 1 | digitale Werkzeuge | e | ka | e | ka | ka | ka |
| 1 | Stadtbezirks-Finanzmittel | e | ka | ka | e | e | ka |
| 2 | Stadtrat-Streams | ka | e | e | e | ka | ka |
| 2 | e-Petition | ka | e | e | e | ka | e |
| 2 | transparente Verwaltung | e | ka | e | ka | ka | ka |
| 2 | virtuelle Bürger:innen-Versammlungen | ka | ka | ka | ka | ka | e |
| 3 | Fediverse | e | ka | + | th | ka | ka |
| 3 | städtisch betriebene Dienste | e | ka | e | th | e | ka |
| 4 | Vendor-Lock-in-Effekt | e | ka | e | ka | e | ka |
| 4 | offene, barrierefreie Formate | ka | ka | e | ka | e | ka |
| 4 | Zusammenarbeit zwischen Kommunen | ka | ka | e | ka | ka | ka |
| 5 | Schulbildung | e | e | ka | e | e | e |
| 5 | Außerschulische Bildung | e | ka | e | e | e | e |
| 5 | Lebenslanges Lernen | ka | ka | e | ka | ka | e | 
| 6 | Zusatzstelle für IT | e | e | e | e | e | e |
| 6 | Bildung aller Lehrkräfte | ka | ka | + | ka | ka | ka |
| 6 | Finanzierung | e | ka | e | ka | ka | e |
| 6 | bestehende IT-Infrastruktur | ka | ka | e | + | ka | ka |
| 7 | Klimaneutralität bis 2035 | e | ka | e | ka | e | fck |
| 7 | Klimaneutralität nach 2035 | ka | ka | ka | ka | e | fck |
| 7 | Modellprojekt Ziel 2030 | ka | ka | e | ka | ka | fck |
| 7 | Aufgabenverteilung der Umsetzung | + | ka | ka | e | e | fck |
| 8 | Werbe- und Bildungsmaßnahmen | e | th | e | ka | e | ka |
| 8 | Möglichkeiten zur (Selbst-) Reparatur | e | th | e | ka | ka | ka |
| 8 | Beschaffungen der Stadt | ka | th | ka | e | e | e |
| 9 | Open Source Software | ka | ka | + | ka | ka | ka |
| 9 | Cyber-Kriminalität | ka | e | ka | ka | e | e |
| 9 | sichere Bürger:innen-Kommunikation | ka | ka | e | ka | ka | ka |
| 10 | Open Source Software und Selfhosting | e | th | e | ka | ka | ka |
| 10 | Kritik an GAFAM | e | th | e | ka | ka | ka |
| 10 | Kritik an Cloud-Systemen allgemein | e | th | ka | e | e | e |
| 11 | Bereitstellung öffentlicher Räume | e | th | e | ka | + | ka |
| 11 | finanzielle Unterstützung | e | th | e | ka | e | ka |
| 12 | Transparenz und Austausch | e | ka | e | ka | e | ka |
| 12 | Kritik an freier Software | ka | e | ka | ka | ka | e |
