# WP OB 2022
Sharepic-Generator für die Wahlprüfsteine von Bits & Bäume Dresden zur OB-Wahl 2022

[Kontext](https://dresden.bits-und-baeume.org/2022/04/28/wahlpruefsteine.html) |
[Ergebnisse auf Mastodon](https://dresden.network/@BitsUndBaeumeDresden/108418734884229955)

## Design-Prinzip
- Quick and Dirty
- SVG-templates, in denen im Text Schlüsselbegriffe mit `sed` austauschen
- `inkscape` und imagemagick (`convert`) voodoo zum rendern

Das ist einmal-Software, bitte nicht nochmal benutzen.

## Verwendung
Mit Python `bin/generate.py` aufrufen, dass erzeugt Shell-Befehle.
Die dann ausführen (i.e. nach `sh` pipen).

```bash
python bin/generate.py | sh
```

## Lizenz
[GPLv3+](./COPYING)

